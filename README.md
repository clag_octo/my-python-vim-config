# My Vim config for Python

## My needs:
- Editor
- Python
- Python environment manager
- a completion tool/intellisense engine using LSP
- a linter/static code analyzer
- a Python style checker
- a documentation style checker
- a formatter (including a fixer)
- a type checker
- “Go-to definition” (& co) feature
- File Browser
- status/tabline
- Complexity checker
- Other computed metrics?
- a snippets tool?
- Other IDE tooling?
- Test integration
- Fuzzy Text File Search?
- Git integration?

## My solution:
- Editor: Neovim
- Python env manager: Conda
- Vim plugins: Vim-plug
- Completion: coc + coc-python
- General tool to check code: Pylama with:
    - linter: Pylint,  Pyflakes?
    - style checker: PyCodeStyle (ex-pep8)
    - documentation style checker: PyDocStyle
    - formatter & fixer: Black (why) & coc-python?
    - complexity checker: Mccabe
- Type checker : Pyright & coc extension coc-pyright
- “Go-to definition” : coc
- File Browser:NERD Tree
- status/tabline: Airline


## Configurations:
- install i-term2 (optional)
- install Python3
- install Conda
- install neovim
- install vim plug
- install vim Coc plugin
- install coc extensions (coc-python & coc-pyright):
    - ex-command :CocInstall coc-python coc-pyright
- create & activate conda env:
    - conda create --name MY_ENV
    - conda activate MY_ENV
    - conda install PYTHON_LIBRARIES...
- create coc config json file named coc-settings.json
    - vim “ex” command :CocConfig
    - Configuration:
        - "coc.preferences.formatOnType": true
        - "coc.preferences.formatOnSaveFiletypes": ["python"]
        - "python.pythonPath": "/Users/USERNAME/anaconda3/bin/python3"
        - "python.condaPath": "/Users/USERNAME/anaconda3/bin"
        - "python.venvPath": "/Users/USERNAME/anaconda3/envs"
        - "python.venvFolders": ["/Users/USERNAMEr/anaconda3/envs/datas/lib/pythonX.X/site-packages"]
        - "python.formatting.provider": "black"
        - "python.linting.pylamaEnabled": true,
        - "python.linting.pylamaArgs": ["-o/Users/USERNAME/pylama.ini"]-
- create a pylama.ini file:
    [pylama:pycodestyle]
    max_line_length = 90

## Other options to check:
- ALE (complementary to coc?)
- fzf.vim

## TODO:
- My Vim config for Javascript&TS/C++/Haskell…
- Quokka like pluggin for vim? Codi, File watcher, Live Reload.
- test Neo Vim (VS Code Neovim)


